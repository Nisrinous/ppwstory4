from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message

def home(request):
    return render(request, 'story3home.html')

def profile(request):
    return render(request, 'story3profile.html')

def activities(request):
    form = Message_Form(request.POST)
    message = Message.objects.all()
    if request.method == 'POST' and form.is_valid():
        data = form.cleaned_data
        activity = Message.objects.create(**data)
        form = Message_Form()
    else:
        form = Message_Form()

    activity = Message.objects.all()
    return render(request, 'story3activities.html', {'message_form': form, 'message': message})

def register(request):
    return render(request, 'story3register.html')

def delete(request):
    if request.method == 'POST':
        deleting = Message.objects.all().delete()

    return HttpResponseRedirect("/activities/")
