from django.db import models
from datetime import datetime

class Message(models.Model):

    activity = models.CharField(max_length=40)
    day = models.CharField(max_length=10)
    date = models.DateTimeField()
    location = models.CharField(max_length=50)
    category = models.CharField(max_length=30)

    def __str__(self):
        return self.message
