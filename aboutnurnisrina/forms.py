from django import forms
class Message_Form(forms.Form):

    # DAY_CHOICES = ( ('monday', 'Monday'),
    #                 ('tuesday', 'Tuesday'),
    #                 ('wednesday', 'Wednesday'),
    #                 ('thursday', 'Thursday'),
    #                 ('friday', 'Friday'),
    #                 ('saturday', 'Saturday'),
    #                 ('sunday', 'Sunday'),
    #               )
    error_messages = {
        'required': 'Please enter this field',
        'invalid': 'Please enter a valid date',
    }
    attrs = {
        'class': 'form-control'
    }

    activity = forms.CharField(label="Activity's name", required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    day = forms.CharField(label='Day', required=True, max_length=10, widget=forms.TextInput(attrs=attrs))
    date = forms.DateTimeField(label='Date and Time', required=True, widget=forms.DateTimeInput(attrs=attrs))
    location = forms.CharField(label='Location', required=False, max_length=50, empty_value='TBA', widget=forms.TextInput(attrs=attrs))
    category = forms.CharField(label='Category', required=False, max_length=30, empty_value='None', widget=forms.TextInput(attrs=attrs))
