from django.urls import path
from .views import home, profile, activities, register, delete
#url for app
urlpatterns = [
    path('', home, name='home'),
    path('profile/', profile, name='profile'),
    path('activities/', activities, name='activities'),
    path('register/', register, name='register'),
    path('activities/delete/', delete, name='delete'),
]
